package Ver_2020.Cviceni_05;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class BasicDrawing {
    public static void main(String[] args) {
        JFrame win = new JFrame();

        makeGui(win);
        win.setTitle("Cv_05 Kravtsov Dmytro A19B0107P");
        win.pack();
        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        win.setLocationRelativeTo(null);
        win.setVisible(true);
    }

    private static void makeGui(JFrame win) {
        DrawingPanel panel = new DrawingPanel();
        panel.setPreferredSize(new Dimension(640, 480));
        win.setLayout(new BorderLayout());
        win.add(panel, BorderLayout.CENTER);

        JButton btnExit = new JButton("Exit");
        JButton btnLarger = new JButton("Larger");
        JButton btnReset = new JButton("R");

        JPanel buttons = new JPanel();

        buttons.add(btnLarger);
        buttons.add(btnExit);
        buttons.add(btnReset);

        win.add(buttons, BorderLayout.SOUTH);

        btnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                win.dispose();
            }
        });

        btnLarger.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                panel.makeStarLarger();
            }
        });

        btnReset.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                panel.resetStar();
            }
        });

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
            @Override
            public boolean dispatchKeyEvent(KeyEvent keyEvent) {
                if (keyEvent.getKeyChar() == '+') {
                    panel.makeStarLarger();
                }
                return false;
            }
        });
    }
}
