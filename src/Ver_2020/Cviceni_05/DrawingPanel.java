package Ver_2020.Cviceni_05;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;

public class DrawingPanel extends JPanel {
    private int N = 5; //pocet cipu hvezdicky
    private final double INIT_R = 200;
    private double R = INIT_R;// velikost hvezdicky
    private final Color INIT_COLOR = Color.YELLOW;
    private Color currentColor = INIT_COLOR;
    Path2D star = null;

    public DrawingPanel() {
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (star != null && star.contains(mouseEvent.getPoint())) {
                    R = R * 0.5;
                    repaint();
                }
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                if (star != null && star.contains(mouseEvent.getPoint())) {
                    currentColor = Color.BLUE;
                    repaint();
                }
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                if (star != null && star.contains(mouseEvent.getPoint())) {
                    currentColor = INIT_COLOR;
                    repaint();
                }
            }
        });

        this.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseMoved(MouseEvent mouseEvent) {
                if (star != null && star.contains(mouseEvent.getPoint())) {
                    currentColor = Color.BLUE;
                    repaint();
                } else {
                    currentColor = INIT_COLOR;
                    repaint();
                }
            }
        });
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D g2 = (Graphics2D)graphics;
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, this.getWidth(), this.getHeight());
        g2.setColor(currentColor);

        drawStar(g2);
    }

    private void drawStar(Graphics2D g2) {
        double R2 = R * 0.5;
        double fi = 2 * Math.PI / N;

        star = new Path2D.Double();
        star.moveTo(R, 0);

        for (int i = 0; i < N; i++) {
            double x = R2 * Math.cos(i * fi + fi * 0.5);
            double y = R2 * Math.sin(i * fi + fi * 0.5);
            star.lineTo(x, y);

            x = R * Math.cos((i + 1) * fi);
            y = R * Math.sin((i + 1) * fi);
            star.lineTo(x, y);
        }
        AffineTransform at = new AffineTransform();

        at.translate(this.getWidth() / 2, this.getHeight() / 2);
        at.rotate(-Math.PI * 0.5);

        star.transform(at);
        g2.fill(star);
    }

    public void makeStarLarger() {
        this.R *= 1.25;
        this.repaint();
    }

    public void resetStar() {
        this.R = INIT_R;
        this.repaint();
    }
}
