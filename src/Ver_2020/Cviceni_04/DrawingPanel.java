package Ver_2020.Cviceni_04;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class DrawingPanel extends JPanel {
    private long startTime = System.currentTimeMillis();
    private int currentX = 0;

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D g2 = (Graphics2D)graphics;

        final int GS = 30;// Ground size
        g2.setColor(Color.GREEN);
        g2.fillRect(0, this.getHeight() - GS, this.getWidth(), GS);
        g2.translate(0, this.getHeight() - GS);

        drawTractor(g2);
        if (currentX < this.getWidth()) {
            currentX += 5;
        } else {
            currentX = -700; //Rozmer nevetsiho kola * 2;
        }
    }

    private void drawTractor(Graphics2D g2) {
        final double R1 = 150;
        final double R2 = 100;
        final double D = 350;

        //Kabinka
        final double L = 20;
        final double W = 300;
        final double H = 400;

        //Motor
        final double MW = 300;
        final double MH = 200;

        double U = R2 / 2;


        g2.setColor(Color.RED);
        g2.fill(new Rectangle2D.Double(L + currentX, -U - H, W, H));
        g2.fill(new Rectangle2D.Double((L + W) + currentX, -U - MH, MW, MH));

        drawWheels(R1, R2, D, g2);
    }

    private void drawWheels(double R1, double R2, double d, Graphics2D g2) {
        g2.translate(R1, -R1);
        drawWheel(R1, g2);
        g2.translate(d, R1 - R2);
        drawWheel(R2, g2);
    }


    private void drawWheel(double R, Graphics2D g2) {
        double R2 = 0.8 * R; //Polomer disku.
        final double R3 = 20; // Polomer oje.
        final double D = 35;
        final double RB = 5;

        g2.setColor(Color.BLACK);
        g2.fill(new Ellipse2D.Double(-R+currentX, -R, 2 * R, 2 * R));

        g2.setColor(Color.DARK_GRAY);
        g2.fill(new Ellipse2D.Double(-R2+currentX, -R2, 2 * R2, 2 * R2));

        g2.setColor(Color.RED);
        g2.fill(new Ellipse2D.Double(-R3+currentX, -R3, 2 * R3, 2 * R3));

        AffineTransform oldTR = g2.getTransform();

        long currentTime = System.currentTimeMillis();
        long elapsedTime = currentTime - startTime;

        final double SPA = Math.PI; //O kolik se otocit za sekundu.

        g2.translate(currentX, 0);
        g2.rotate(Math.toRadians(45) + SPA * elapsedTime / 1000.0);
        for (int i = 0; i < 4; i++) {
            g2.translate(D, 0);
            g2.fill(new Ellipse2D.Double(-RB, -RB, 2 * RB, 2 * RB));
            g2.translate(-D, 0);
            g2.rotate(Math.toRadians(90));
        }

        g2.setTransform(oldTR);
    }
}
