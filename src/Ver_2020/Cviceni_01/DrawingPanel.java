package Ver_2020.Cviceni_01;

import javax.swing.*;
import java.awt.*;

public class DrawingPanel extends JPanel {
    private static final long serialVersionUID = 1L;

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, this.getWidth(), this.getHeight());

        g2.setColor(Color.BLACK);

        int circleRadius = 200;
        g2.drawOval(this.getWidth() / 2 - circleRadius, this.getHeight() / 2 - circleRadius, 2 * circleRadius, 2 * circleRadius);

        int[] xPoints = new int[]{this.getWidth() / 2 - circleRadius / 2, //bottom_left
                                  this.getWidth() / 2, //top_middle
                                  this.getWidth() / 2 + circleRadius / 2, //bottom_right
                                  this.getWidth() / 2 - circleRadius, //middle_left
                                  this.getWidth() / 2 + circleRadius}; //middle_right

        int[] yPoints = new int[]{this.getHeight() / 2 + circleRadius,
                this.getHeight() / 2 - circleRadius,
                this.getHeight() / 2 + circleRadius,
                this.getHeight() / 2 - circleRadius / 3,
                this.getHeight() / 2 - circleRadius / 3};

        g2.drawPolygon(xPoints, yPoints, xPoints.length);


        //First part of the lesson.

        /*final int CX = this.getWidth() - 200;
        final int CY = this.getHeight() - 100;
        final int D = 100;
        g2.fillOval(CX - D / 2, CY - D / 2, D, D);

        g2.setColor(Color.BLUE);

        int[] xPoints = new int[]{CX - D / 2, CX, CX + D / 2, CX};
        int[] yPoints = new int[]{CY, CY - D / 2, CY, CY + D / 2};

        g2.fillPolygon(xPoints, yPoints, xPoints.length);
        g2.drawRect(CX - D / 2, CY - D / 2, D, D);

        g2.setFont(new Font("Arial", Font.ITALIC, 32));
        String s = "Nejaky muj texticek";
        FontMetrics fm = g2.getFontMetrics();
        g2.drawString(s, CX - fm.stringWidth(s) / 2, CY + D);*/
    }
}
