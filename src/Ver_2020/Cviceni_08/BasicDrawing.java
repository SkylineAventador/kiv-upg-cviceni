package Ver_2020.Cviceni_08;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BoxAndWhiskerRenderer;
import org.jfree.data.statistics.DefaultBoxAndWhiskerCategoryDataset;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BasicDrawing {
    public static void main(String[] args) {
        JFrame win = new JFrame();
        win.setTitle("CV_08 A19B0107P KRAVTSOV Dmytro");

        ChartPanel chartPanel = new ChartPanel(
                makeBarChart()
        );

        win.add(chartPanel);
        win.pack();

        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        win.setLocationRelativeTo(null);
        win.setVisible(true);
    }

    private static JFreeChart makeBarChart()
    {
        DefaultBoxAndWhiskerCategoryDataset dataset = new DefaultBoxAndWhiskerCategoryDataset();
        ArrayList<String[]> loadedData = loadData("Data2.txt");

        boolean isFirst = true;
        for (Iterator iterator = loadedData.iterator(); iterator.hasNext();) {
            String[] strings = (String[])iterator.next();
            if (isFirst) {
                isFirst = false;
                continue;
            }

            final List<Double> innerList = new ArrayList<>(strings.length);
            double currentSum = 0;
            for (int i = 1; i < strings.length; i++) {
                double value = Double.parseDouble(strings[i]);
                innerList.add(value);
                currentSum += value;
            }
            dataset.add(innerList, currentSum, strings[0].substring(0, 5));
        }

        JFreeChart chart = ChartFactory.createBoxAndWhiskerChart("COVID-19", "Den", "Pocet infikovanych na 100 tis. obyvatel", dataset, false);
        CategoryPlot plot = chart.getCategoryPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.GRAY);

        BoxAndWhiskerRenderer renderer = (BoxAndWhiskerRenderer) plot.getRenderer();
        renderer.setItemMargin(-5);

        return chart;
    }

    private static ArrayList<String[]> loadData(String fileName) {
        ArrayList<String[]> list = new ArrayList<>();

        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(new File(fileName)));

            String line;
            while((line = br.readLine()) != null) {
                list.add(line.split("\\s"));
            }

            br.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return list;
    }
}
