package Ver_2020.Cviceni_07;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.*;
import java.awt.geom.Ellipse2D.Double;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

import javax.swing.JPanel;

public class DrawingPanel extends JPanel implements Printable {
	public DrawingPanel() {
		this.setPreferredSize(new Dimension(600, 480));
	}
	
	@Override
	public void paint(Graphics g) {	
		//super.paint(g);
		
		Graphics2D g2 = (Graphics2D)g;
		g2.setColor(Color.WHITE);
		g2.fillRect(0, 0, this.getWidth(), this.getHeight());

		g2.translate(this.getWidth() / 2, this.getHeight() / 2);
		//drawPacman(200, g2);
		drawBonusButton(500, g2);
	}

	private void drawPacman(double r, Graphics2D g2) {
		double ro = r * 0.25;
		double ri = ro * 0.5;
		double ecx = 0;
		double ecy = -r * 0.5;

		Path2D eye = new Path2D.Double();

		//Whole outer eye
		eye.moveTo(ecx + ro, ecy);
		eye.lineTo(ecx, ecy + ro);
		eye.lineTo(ecx - ro, ecy);
		eye.lineTo(ecx, ecy - ro);
		eye.closePath();

		//Inner eye
		eye.moveTo(ecx + ri, ecy);
		eye.lineTo(ecx, ecy - ri);
		eye.lineTo(ecx - ri, ecy);
		eye.lineTo(ecx, ecy + ri);
		eye.closePath();

		Area pacman = new Area(new Ellipse2D.Double(-r, -r, 2 * r, 2 * r));
		pacman.subtract(new Area(eye));

		Path2D mouth = new Path2D.Double();
		mouth.moveTo(2 * r, -0.5 * r);
		mouth.lineTo(0, 0);
		mouth.lineTo(2 * r, 0.5 * r);
		mouth.closePath();

		pacman.subtract(new Area(mouth));

		//Gradient pacman
		g2.setPaint(new RadialGradientPaint(new Point2D.Double(0, 0),
				(float) (2 * r),
				new float[]{0, 1},
				new Color[]{Color.YELLOW, Color.RED}));
		g2.fill(pacman);

		//Shadow
		g2.setPaint(new LinearGradientPaint(new Point2D.Double(0, 0),
				new Point2D.Double(0, r),
				new float[]{0, 0.8f, 1},
				new Color[]{ //Semi transparent colors
						new Color(0, 0, 0, 0),
						new Color(0, 0, 0, 0.2f),
						new Color(0, 0, 0, 0.8f)
				}));
		g2.fill(pacman);

		g2.setClip(pacman);
		g2.setColor(Color.RED);

		final int delta = 50;
		for (int y = (int) -r; y < r; y += delta) {
			for (int x = (int) -r; x < r; x += delta) {
				g2.fillOval(x, y, 10, 10);
			}
		}

	}

	private void drawBonusButton(double size, Graphics2D g2){
		Area button = new Area(new RoundRectangle2D.Double(-size / 2, -size / 4, size, size / 2, 50, 50));
		String text = "TEXT";

		g2.fill(button);

		g2.setClip(button);
		g2.setFont(new Font("Arial", Font.PLAIN, 96));


		final int delta = 50;
		for (int y = (int) -size; y < size; y += delta) {
			for (int x = (int) -size; x < size; x += delta) {
				g2.setPaint(new RadialGradientPaint(new Point2D.Double(x + (delta/2), y + (delta/2)),
						(float) (delta),
						new float[]{0, 1},
						new Color[]{Color.GRAY, Color.WHITE}));
				g2.fillRect(x, y, delta, delta);
			}
		}
		g2.drawString(text, (int) -size/4, (int) 36);

	}

	@Override
	public int print(Graphics graphics, PageFormat pageFormat, int i) throws PrinterException {
		if (i > 0){
			return NO_SUCH_PAGE;
		}
		Graphics2D g2 = (Graphics2D)graphics;
		g2.translate(pageFormat.getWidth() / 2, pageFormat.getHeight() / 2);

		//Pacman size in physical units
		//Transform to points.
		//1 point = 1/72 in.
		//1 in    = 25.4 mm.
		//1 pt    = 25.4/72 mm.
		//1 mm    = 72/25.4 mm.
		final double mmToPt = (72 / 25.4);
		drawPacman(50 * mmToPt, g2);
		return 0;
	}
}
