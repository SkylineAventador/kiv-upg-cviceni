package Ver_2020.sandbox;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

/**
 * @author Dmytro Kravtsov 22.06.2020
 * Cvicna trida pro kresleni zadaneho poctu kruhu, stred kterych lezi na "hlavni" kruznici.
 */
public class DrawingPanel extends JPanel {

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        Graphics2D g2 = (Graphics2D)graphics;

        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, this.getWidth(), this.getHeight());

        g2.setColor(Color.BLACK);

        //Functional code.
        g2.setStroke(new BasicStroke(3f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));

        double initx, inity, initr;
        initx = this.getWidth() / 2.0; // Center X
        inity = this.getHeight() / 2.0; // Center Y
        initr = 500; // Circle radius

        g2.setColor(Color.RED);
        g2.drawLine((int) initx, 0, (int) initx, this.getHeight());
        g2.drawLine(0, (int) inity, this.getWidth(), (int) inity);
        g2.setColor(Color.BLACK);

        g2.translate(initx - initr / 2, inity - initr / 2); // Translation to the window center.

        final int NUM_POINTS = 50;
        for (int i = 0; i < NUM_POINTS; ++i) {
            final double angle = Math.toRadians(((double) i / NUM_POINTS) * 360d);
            paintCircle(g2, Math.cos(angle) * initr / 2, Math.sin(angle) * initr / 2, initr);
        }

    }

    private void paintCircle(Graphics2D g2, double x, double y, double r) {
        Ellipse2D circle = new Ellipse2D.Double(x, y, r, r);
        g2.draw(circle);
    }
}
