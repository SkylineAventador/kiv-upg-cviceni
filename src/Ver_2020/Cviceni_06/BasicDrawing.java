package Ver_2020.Cviceni_06;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class BasicDrawing {
    public static void main(String[] args) {
        JFrame win = new JFrame();

        makeGui(win);
        win.setTitle("Cv_06 Kravtsov Dmytro A19B0107P");
        win.pack();
        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        win.setLocationRelativeTo(null);
        win.setVisible(true);
    }

    private static void makeGui(JFrame win) {
        DrawingPanel panel = new DrawingPanel();
        panel.setPreferredSize(new Dimension(640, 480));
        win.setLayout(new BorderLayout());
        win.add(panel, BorderLayout.CENTER);

        try {
            panel.loadImage("./src/Ver_2020/Cviceni_06/data/1000x1000.jfif"); //Lokalne bylo umisteno v rootu projektu.
            panel.loadImage_Bonus("./src/Ver_2020/Cviceni_06/data/Green-Screen-Lighting.jpg");

            //Bonus
            //panel.saveImage("pokus.jfif", 1280, 720);
            panel.saveImage_Bonus("Bonus.jfif", 1280, 720);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
