package Ver_2020.Cviceni_11;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Ellipse2D.Double;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class DrawingPanel extends JPanel {

	private long startTime = System.currentTimeMillis();
	private BufferedImage outputFrame;
	private int currentFrameIndex = 0;
	
	@Override
	public void paint(Graphics g) {	
		super.paint(g);
		Graphics2D g2 = (Graphics2D)g;

		drawOutputFrame(outputFrame);
		g2.drawImage(outputFrame, 0, 0, outputFrame.getWidth(), outputFrame.getHeight(), null);
		saveCurrentFrame(outputFrame, "./CV_11_Frames");

		if (currentFrameIndex < 10000) {
			currentFrameIndex++;
		} else {
			System.err.println("Error: Maximum frames overflow.");
			System.exit(1);
		}
	}

	private void saveCurrentFrame(BufferedImage currentFrame, String savePath) {
		String fileName = "" + currentFrameIndex;
		String zeros = "000";
		if (currentFrameIndex >= 10 && currentFrameIndex < 100) {
			zeros = "00";
		} else if (currentFrameIndex >= 100 && currentFrameIndex < 1000) {
			zeros = "0";
		} else if (currentFrameIndex > 1000){
			zeros = "";
		}
		try {
			File outputFile = new File(savePath + File.separator + zeros + fileName + ".jpg");
			outputFile.getParentFile().mkdirs();
			outputFile.createNewFile();
			ImageIO.write(currentFrame, "jpg", outputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void drawOutputFrame(BufferedImage outputFrame) {
		Graphics2D g2 = outputFrame.createGraphics();

		g2.setColor(Color.WHITE);
		g2.fillRect(0, 0, this.getWidth(), this.getHeight());

		final int GS = 30;
		g2.setColor(Color.GREEN);
		g2.fillRect(0, this.getHeight() - GS, this.getWidth(), GS);
		g2.translate(0, this.getHeight() - GS);

		drawTractor(g2);
	}


	private void drawTractor(Graphics2D g2) {
		long currTime = System.currentTimeMillis();
		long elapsed = currTime - startTime;
		g2.translate(elapsed*0.1, 0);
		
		
		final double R1 = 150;
		final double R2 = 100;
		final double D = 350;
		
		final double L = 20;
		final double W = 300;
		final double H = 400;
		final double MW = 300;
		final double MH = 200;
		double U = R2 / 2;
		
		g2.setColor(Color.RED);
		g2.fill(new Rectangle2D.Double(L, -U - H, W, H));
		g2.fill(new Rectangle2D.Double(L + W, -U - MH, MW, MH));
		
		drawWheels(R1, R2, D, g2);
	}
	
	private void drawWheels(double R1, double R2, double d, Graphics2D g2) {
		g2.translate(R1, -R1);
		drawWheel(R1, g2);
		g2.translate(d, R1 - R2);
		drawWheel(R2, g2);		
	}
	
	private void drawWheel(double R, Graphics2D g2) {
		double R2 = 0.8 * R;	//polomer disku
		final double R3 = 20;	//polomer oje
		final double D = 45;
		final double RB = 5;	//polomer 
		
		g2.setColor(Color.BLACK);
		g2.fill(new Ellipse2D.Double(-R, -R, 2*R, 2*R));	
		
		g2.setColor(Color.DARK_GRAY);
		g2.fill(new Ellipse2D.Double(-R2, -R2, 2*R2, 2*R2));
				
		g2.setColor(Color.RED);
		g2.fill(new Ellipse2D.Double(-R3, -R3, 2*R3, 2*R3));
		
		AffineTransform oldTR = g2.getTransform();

		long currTime = System.currentTimeMillis();
		long elapsed = currTime - startTime;
		final double SPA = Math.PI;
		
		g2.rotate(Math.toRadians(45) + SPA*elapsed / 1000.0);
		for (int i = 0; i < 4; i++) {
			g2.translate(D, 0);
			g2.fill(new Ellipse2D.Double(-RB, -RB, 2*RB, 2*RB));
			g2.translate(-D, 0);
			g2.rotate(Math.toRadians(90));
		}
		
		g2.setTransform(oldTR);
	}

	public void inititalizeImage() {
		outputFrame = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
	}
}
