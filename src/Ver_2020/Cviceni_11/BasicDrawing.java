package Ver_2020.Cviceni_11;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;


public class BasicDrawing {
	// TODO: 08.04.2020 Obrazek 1700 x 700
	// TODO: 08.04.2020 Poradove cislo xxxx

	// TODO: 08.04.2020 Video vystupni vystup.mp4
	// TODO: 08.04.2020 1600 x 900
	// TODO: 08.04.2020 Bude pridano osobni cislo a jmeno v horni casti pomoci graphics magick.
	// TODO: 08.04.2020 V dolni casti budou 8 obrazku 200x200 jako sekvence snimku u Applu.
	// TODO: 08.04.2020 Pomoci graphics magick vsechny 3 komponenty slozit dohromady a udelat 1 snimek a dat do exportu.
	// TODO: 08.04.2020 Pro generovani pouzit ffmpeg. Z mnoziny snimku.
	public static void main(String[] args) {
		JFrame win = new JFrame();
		win.setTitle("A19B0107P Dmytro KRAVTSOV CV_11");
				
		DrawingPanel panel = new DrawingPanel();
		panel.setPreferredSize(new Dimension(1600, 700));
		win.add(panel);
		win.pack();

		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		win.setLocationRelativeTo(null);

		panel.inititalizeImage();

		win.setVisible(true);
		
				
		Timer myTimer = new Timer();
		myTimer.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {				
				panel.repaint();
			}
		}, 0, 20);
		
		win.addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				System.out.println("CONVERTING ...");
				try {
//					//ZDE spustte vse potrebne
//					Runtime.getRuntime()
//					.exec("gm convert logo: logo.jpg") //zmente z prikladu na rozumne
//					.waitFor();

					//ZDE spustte vse potrebne
					Runtime.getRuntime()
							.exec("gm mogrify -font Arial -pointsize 36 -draw \"text 1000,100 'A19B0107P Dmytro KRAVTSOV'\" " +
									"-format jpg -output-directory CV_11_Frames C:\\Users\\user\\IdeaProjects\\kiv-upg-cviceni\\CV_11_Frames\\*.jpg") //zmente z prikladu na rozumne
							.waitFor();

					Runtime.getRuntime()
							.exec("gm mogrify -geometry \"200x200^\" -gravity center -extent 200x200 -create-directories " +
									"-output-directory CV_11_Thumbs -format jpg CV_11_Frames\\*.jpg")
							.waitFor();

					Runtime.getRuntime()
							.exec("ffmpeg -framerate 10 -i CV_11_Frames\\%04d.jpg -c:v h264 -crf 19 -maxrate 2000k vystup.mp4")
							.waitFor(); // TODO: 08.04.2020 Pokracovat dale tady.
//					//a na samy zaver vysledek zobrazime
//					Runtime.getRuntime()
//					.exec("cmd /c start /wait logo.jpg") //zmente z prikladu na rozumne
//					.waitFor();

					System.out.println("EVERYTHING DONE. EXIT.");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				System.out.println("WINDOW CLOSED");
				
			}
			
			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
}
