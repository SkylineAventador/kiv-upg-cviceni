package Ver_2020.Cviceni_03;

import Ver_2020.ArchiveMethods;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

public class DrawingPanel extends JPanel {
    private static final long serialVersionUID = 1L;

    final double[] House_X = new double[]{10085.12, 10150.35, 10120.20, 10079.30};
    final double[] House_Y = new double[]{40.2, 60.3, 100.11, 90.45};

    final double WorldMinX = 10079.30;
    final double WorldMinY = 40.2;

    final double WorldMaxX = 10150.35;
    final double WorldMaxY = 100.11;

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D)g;

        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, this.getWidth(), this.getHeight());

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2.setColor(Color.BLACK);


        // TODO: 06.03.2020 This block is good for SP scaling.
        double scaleX = this.getWidth() / (WorldMaxX - WorldMinX);
        double scaleY = this.getHeight() / (WorldMaxY - WorldMinY);

        double scale = Math.min(scaleX, scaleY);

        int x[] = new int[House_X.length];
        int y[] = new int[House_Y.length];

        for (int i = 0; i < x.length; i++) {
            x[i] = (int) ((House_X[i] - WorldMinX) * scale);
            y[i] = (int) ((House_Y[i] - WorldMinY) * scale);
        }
        //=====================================================
        g2.setStroke(new BasicStroke(10));
        g2.drawPolygon(x, y, x.length);


//        g2.setStroke(new BasicStroke(5));
//        //ArchiveMethods.drawLinesAndCircle(g2, this.getWidth(), this.getHeight());
//        int CX = this.getWidth() / 2;
//        int CY = this.getHeight() / 2;
//        final double D = 20;
//
//        double scaleX = this.getWidth() / D;
//        double scaleY = this.getHeight() / D;
//
//        double scale = Math.min(scaleX, scaleY);
//        /*
//        g2.setStroke(new BasicStroke(3));
//
//        g2.drawOval((int) (CX - (D / 2) * scale),
//                (int) (CY - (D / 2) * scale),
//                (int) (D * scale),
//                (int) (D * scale));
//         */
//
//        Ellipse2D circle = new Ellipse2D.Double(CX - (D / 2) * scale, CY - (D / 2) * scale, D * scale, D * scale);
//        g2.setColor(Color.RED);
//        g2.fill(circle);
//
//        g2.setColor(Color.BLACK);
//        ArchiveMethods.drawArrow(g2, CX - (D / 2) * scale, CY, CX + (D / 2) * scale, CY, 30);
//        ArchiveMethods.drawArrow(g2, CX, CY - (D / 2) * scale, CX, CY + (D / 2) * scale, 30);
//
//        ArchiveMethods.drawArrow(g2, 0, this.getHeight(), this.getWidth(), 0, 30);

    }
}
