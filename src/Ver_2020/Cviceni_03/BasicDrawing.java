package Ver_2020.Cviceni_03;

import javax.swing.*;
import java.awt.*;

public class BasicDrawing {
    public static void main(String[] args) {
        JFrame frame = new JFrame();

        DrawingPanel drawingPanel = new DrawingPanel();
        drawingPanel.setPreferredSize(new Dimension(640, 480));
        frame.add(drawingPanel);
        frame.setTitle("Cv_03 Kravtsov Dmytro A19B0107P");
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
