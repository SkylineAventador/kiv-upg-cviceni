package Ver_2020;

import java.awt.*;
import java.awt.geom.Line2D;

public class ArchiveMethods {
    public static void drawLinesAndCircle(Graphics2D g2, int width, int height) {
        BasicStroke currentStroke = (BasicStroke)g2.getStroke();

        g2.setStroke(new BasicStroke(10, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10, new float[]{10.0f,  20.0f}, 0));

        g2.drawLine(0, 0, width, height);
        g2.drawLine(0, height, width, 0);
        g2.drawLine(0, height / 2, width, height / 2);
        g2.drawLine(width / 2, 0, width / 2, height);
        int circleSize = (int) (width * 0.3 + height * 0.3);
        g2.drawOval(width / 2 - circleSize / 2, height / 2 - circleSize / 2, circleSize, circleSize);

        g2.setStroke(currentStroke);
    }

    public static void drawArrow(Graphics2D g2, double x1, double y1, double x2, double y2, double arrowLength) {
        // Spocitame slozky vektoru od (x1, y1) k (x2, y2)
        double vx = x2 - x1;
        double vy = y2 - y1;

        // Spocitame vektoru v, tj usecky od (x1, y1) k (x2, y2).
        // K vypoctu druhe mocniny idealne pouzivame nasobeni, ne funkci pow
        // (je mnohem pomalejsi).
        double vLength = Math.sqrt(vx*vx + vy*vy);

        // Z vektoru v udelame vektor jednotkove delky
        double vNormX = vx / vLength;
        double vNormY = vy / vLength;

        // Vektor v protahneme na delku arrowLength
        double vArrowX = vNormX * arrowLength;
        double vArrowY = vNormY * arrowLength;

        // Spocitame vektor kolmy k (vx, vy)
        // Z nej pak odvodime koncove body carek tvoricich sipku.
        double kx = -vArrowY;
        double ky = vArrowX;

        // Upravime delku vektoru k, aby byla sipka hezci
        kx *= 0.25;
        ky *= 0.25;

        // Cara od (x1, y1) k (x2, y2)
        g2.draw(new Line2D.Double(x1, y1, x2, y2));

        // Sipka na konci
        g2.draw(new Line2D.Double(x2, y2, x2 - vArrowX + kx, y2 - vArrowY + ky));
        g2.draw(new Line2D.Double(x2, y2, x2 - vArrowX - kx, y2 - vArrowY - ky));

        //Double sipka
        g2.draw(new Line2D.Double(x2 - vArrowX, y2 - vArrowY, x2 - vArrowX*2 + kx, y2 - vArrowY*2 + ky));
        g2.draw(new Line2D.Double(x2 - vArrowX, y2 - vArrowY, x2 - vArrowX*2 - kx, y2 - vArrowY*2 - ky));
    }

    public static void drawFullHeightArrows(Graphics2D g2, int width, int height) {
        double x1 = 20;
        double y1 = 20;
        double x2 = width-20;
        for (double y2 = 20; y2 < height - 20; y2 += 40) {
            ArchiveMethods.drawArrow(g2, x1, y1, x2, y2, 30);
        }
    }
}
