package Ver_2020.Cviceni_10;

import org.jfree.graphics2d.svg.SVGGraphics2D;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BasicDrawing {
    public static void main(String[] args) {
        JFrame win = new JFrame();
        win.setTitle("CV_10 A19B0107P KRAVTSOV Dmytro");

        makeGui(win);
        win.pack();

        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        win.setLocationRelativeTo(null);
        win.setVisible(true);
        /*
        final int ANGLE = 20;
        final int TX = 105;
        final int TY = 0;

        for (int i = 0; i < 360 / ANGLE; i++) {
            System.out.println("<ellipse cx = \"0\" cy = \"0\" rx = \"60\" ry = \"10\" stroke = \"black\" stroke-width = \"3\" fill = \"white\" transform = \"rotate(" + i * ANGLE + "), translate(" + TX + "," + TY + ")\"/>");
        }
        */
    }

    private static void makeGui(JFrame win) {
        DrawingPanel panel = new DrawingPanel();
        win.setLayout(new BorderLayout());
        win.add(panel, BorderLayout.CENTER);

//        SVGGraphics2D g2 = new SVGGraphics2D(750, 750);
        MyGraphics2D g2 = new MyGraphics2D(750, 750);
        panel.drawFlower(200, 200, g2);
        System.out.println(g2.getSVGElement());

        JButton bttnExit = new JButton("Exit");

        JPanel buttons = new JPanel();
        buttons.add(bttnExit);

        win.add(buttons, BorderLayout.SOUTH);

        bttnExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                win.dispose();
            }
        });
    }
}
