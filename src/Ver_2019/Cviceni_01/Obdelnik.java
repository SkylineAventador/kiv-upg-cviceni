package Ver_2019.Cviceni_01;

import java.util.Scanner;

public class Obdelnik {
    private static Scanner scanner = new Scanner(System.in);



    public static void main(String[] args) {
        System.out.println("Zadejte pocet sloupcu: X = ");
        int pocetSloupcu = scanner.nextInt();

        System.out.println("Zadejte pocet radku: Y = ");
        int pocetRadku = scanner.nextInt();


        int yKoef = (pocetRadku - 1) / 2;
        int xKoef = (pocetSloupcu - 1) / 3;

        for (int i = 0; i < pocetRadku; i++) {
            for (int j = 0; j < pocetSloupcu; j++) {
                if (j <= xKoef) {
                    System.out.print("*");
                } else if (i >= yKoef) {
                    System.out.print("*");
                } else {
                    System.out.print(".");
                }

            }
            System.out.println();
        }
    }
}
