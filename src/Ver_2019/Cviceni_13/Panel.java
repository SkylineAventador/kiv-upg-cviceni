package Ver_2019.Cviceni_13;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Panel extends JPanel {
    List<Double> coordinates = new ArrayList<Double>();
    List<Integer> idx = new ArrayList<Integer>();
    private double[] bounds = new double[6];
    private double alfa = Math.PI;

    public Panel() {
        readObjFile("cv13_data/lion.obj", coordinates, idx);
        calculateBounds();
        for (double bound : bounds) {
            System.out.println(bound);
        }
    }


    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        int size = 3;
        int w = getWidth();
        int h = getHeight();

        g2.setColor(Color.RED);
        for (int a = 0; a < idx.size(); a += 3) {
            int i1 = 3 * idx.get(a);
            int i2 = 3 * idx.get(a+1);
            int i3 = 3 * idx.get(a + 2);

            double x1 = coordinates.get(i1);
            double y1 = coordinates.get(i1+1);
            double z1 = coordinates.get(i1+2);

            double x2 = coordinates.get(i2);
            double y2 = coordinates.get(i2+1);
            double z2 = coordinates.get(i2+2);

            double x3 = coordinates.get(i3);
            double y3 = coordinates.get(i3+1);
            double z3 = coordinates.get(i3+2);

            Point2D p1 = convert3Dto2D(w, h, x1, y1, z1);
            Point2D p2 = convert3Dto2D(w, h, x2, y2, z2);
            Point2D p3 = convert3Dto2D(w, h, x3, y3, z3);

            g2.draw(new Line2D.Double(p1, p2));
            g2.draw(new Line2D.Double(p1, p3));
            g2.draw(new Line2D.Double(p2, p3));
        }

        g2.translate(0, h);
        g2.scale(1, -1);

//        for (int a = 0; a < coordinates.size(); a += 3) {
//            double x = coordinates.get(a);
//            double y = coordinates.get(a + 1);
//            double z = coordinates.get(a + 2);
//
//            Point2D p = convert3Dto2D(w, h, x, y, z);
//
//            Rectangle2D point = new Rectangle2D.Double(p.getX() - size * .5, p.getY() - size * .5, size, size);
//            g2.fill(point);
//
//        }
    }

    private Point2D convert3Dto2D(int w, int h, double xp, double yp, double zp) {
        //scale()
        // ((maxo-mino)/(maxs-mins))*(d-mins)-mino;  o - obrazovka, s - svet

        double x = xp;
        double y = yp * Math.cos(alfa) - zp * Math.sin(alfa);

        int padding = 3;
        int maxo_x = (w-padding);
        int mino_x = padding;

        int maxo_y = (h-padding);
        int mino_y = padding;

        double mins_x = bounds[0];
        double maxs_x = bounds[3];
        double mins_y = bounds[1];
        double maxs_y = bounds[4];

        double ret_x = (maxo_x - mino_x) * (x - mins_x) / (maxs_x - mins_x) + mino_x;
        double ret_y = (maxo_y - mino_y) * (y - mins_y) / (maxs_y - mins_y) + mino_y;

        return new Point2D.Double(ret_x, ret_y);
    }

    /**
     * Nacteni jednducheho Wavefront OBJ souboru.
     * Predpokladaji se jen elementy
     * v (souradnice X, Y, Z vrcholu)
     * f (indexy A, B, C trojuhelniku)
     * # (komentar)
     *
     * @param fileName    jmeno vstupniho souboru
     * @param coordinates pole souradnic vsech vrcholu v poradi x, y, z, x, y, z, ...
     * @param faces       indexy do pole coordinates, NA ROZDIL OD OBJ SE INDEXUJE OD 0
     */
    public static void readObjFile(String fileName, List<Double> coordinates, List<Integer> faces) {
        Charset charset = Charset.forName("US-ASCII");
        try (BufferedReader reader = Files.newBufferedReader(Paths.get(fileName), charset)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                line = line.trim();

                // Zahodit prazdne radky a komentare
                if (line.length() == 0 || line.charAt(0) == '#')
                    continue;

                // Rozdelit radku na pole, oddeleni bilymi znaky
                String fields[] = line.split("[ \t]+");

                // Zpracovani elementu
                if (fields[0].compareToIgnoreCase("v") == 0) {
                    // Vrchol (predpokladaji se 3 souradnice)
                    for (int i = 1; i < fields.length; i++) {
                        coordinates.add(Double.parseDouble(fields[i]));
                    }
                } else if (fields[0].compareToIgnoreCase("f") == 0) {
                    // Stena (predpokladaji se 3 indexy)
                    for (int i = 1; i < fields.length; i++) {
                        faces.add(Integer.parseInt(fields[i]) - 1); // -1, protoze OBJ indexuje od 1
                    }
                }
            }
            System.out.println("Read of the file has been done.");
            System.out.println("Size is: " + coordinates.size() + " records");
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
        }
    }

    private void calculateBounds() {
        for (int a = 0; a < 3; a++) {
            bounds[a] = Double.MAX_VALUE;
            bounds[a + 3] =- Double.MAX_VALUE;
        }

        for (int a = 0; a < coordinates.size(); a++) {
            double current = coordinates.get(a);
            int idx_min = a % 3;
            int idx_max = idx_min + 3;
            if (bounds[idx_min] > current) {
                bounds[idx_min] = current;
            }
            bounds[idx_max] = Math.max(current, bounds[idx_max]);
        }
    }
}
