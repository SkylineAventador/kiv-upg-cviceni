package Ver_2019.Cviceni_03;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;

public class Panel extends JPanel {
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        /*
        Rovnice prevodu
        (d-minR)/(maxR-minR)*(maxO-minO)+minO
        */

//        int margin = 10;
//
//        int min_rx = 50;
//        int max_rx = 160;
//
//        int min_ox = margin;
//        int r = Math.min(getWidth(), getHeight());
//        int max_ox = r-margin;
//
//        int dx1 = 70;
//        double vysledek_X1 = (double)(dx1 - min_rx) / (max_rx - min_rx) * (max_ox - min_ox) + min_ox;
//        int dx2 = 120;
//        double vysledek_X2 = (double)(dx2 - min_rx) / (max_rx - min_rx) * (max_ox - min_ox) + min_ox;
//
//
//        int min_ry = 70;
//        int max_ry = 183;
//
//        int min_oy = margin;
//        int max_oy = r - margin;
//
//        int dy1 = 120;
//        double vysledek_Y1 = (double)(dy1 - min_ry) / (max_ry - min_ry) * (max_oy - min_oy) + min_oy;
//        int dy2 = 160;
//        double vysledek_Y2 = (double)(dy2 - min_ry) / (max_ry - min_ry) * (max_oy - min_oy) + min_oy;
//
//
        Graphics2D g2d = (Graphics2D)g;
//        g2d.draw(new Rectangle2D.Double(vysledek_X1, vysledek_Y1, vysledek_X2 - vysledek_X1, vysledek_Y2 - vysledek_Y1));

        //Sipka

        int ax = 0;
        int ay = getHeight();

        int bx = getWidth();
        int by = 0;

        int k = 25;//px
        int l = 5;//px

        drawArrrow(g2d, ax, ay, bx, by, k, l);
        //drawArrrow(g2d, getWidth(), getHeight(), 0, 0, k, l);
    }

    private void drawCross(Graphics2D g) {

    }

    private void drawArrrow(Graphics2D g, int ax, int ay, int bx, int by, int k, int l) {
        g.drawLine(ax, ay, bx, by);

        int ux = ax-bx;
        int uy = ay-by;

        double d = Math.hypot(ux, uy);

        double ux_1 = ux/d;
        double uy_1 = uy/d;

        double cx = bx + k * ux_1;
        double cy = by + k * uy_1;

        double nx_1 = uy_1;
        double ny_1 = -ux_1;

        double d1_x = cx + nx_1 * l;
        double d1_y = cy + ny_1 * l;

        double d2_x = cx - nx_1 * l;
        double d2_y = cy - ny_1 * l;

        g.draw(new Line2D.Double(bx, by, d1_x, d1_y));
        g.draw(new Line2D.Double(bx, by, d2_x, d2_y));


        //Krizek
        g.draw(new Line2D.Double(getWidth()-bx, by*2, d1_x, d1_y));
        g.draw(new Line2D.Double(getWidth()-bx, by*2, d2_x, d2_y));
    }
}
