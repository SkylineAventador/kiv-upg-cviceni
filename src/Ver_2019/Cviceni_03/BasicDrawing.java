package Ver_2019.Cviceni_03;

import javax.swing.*;
import java.awt.*;

public class BasicDrawing {
    public static void main(String[] args) {
        JFrame frame = new JFrame();

        // Vlastni graficky obsah
//        DrawingPanel drawingPanel = new DrawingPanel();
        Panel panel = new Panel();
        panel.setPreferredSize(new Dimension(800, 600));
        frame.add(panel);

        // Standardni manipulace s oknem
        frame.setTitle("Kresleni");
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
