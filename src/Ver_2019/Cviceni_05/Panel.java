package Ver_2019.Cviceni_05;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

/*
4 zakladni operace: posun, rotace, skalovani, zkoseni.
 */
public class Panel extends JPanel {
    private int r = 60;
    private final int MAX_R = 60;
    private Ellipse2D ellipse2D;
    private Color barva = Color.RED;

    private int cursor_x, cursor_y;

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D)g;

        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());


        int w = getWidth();
        int h = getHeight();
//        int w = cursor_x;
//        int h = cursor_y;
        g.setColor(barva);

        ellipse2D = new Ellipse2D.Double(w - r, h - r, 2 * r, 2 * r);
        g2.fill(ellipse2D);

    }


    public void setCursorCoords(int x, int y) {
        cursor_x = x;
        cursor_y = y;

    }

    public void setBarva(Color barva) {
        this.barva = barva;
    }

    public boolean isInsideEllipse(int x, int y) {
        return ellipse2D.contains(x, y);
    }

    public void zmensi() {
        r>>=1;
        repaint();
    }

    public void reset() {
        r = MAX_R;
        repaint();
    }

}
