package Ver_2019.Cviceni_05;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BasicDrawing {
    private static boolean predchozi = true;
    private static Color initColor;
    public static void main(String[] args) {
        JFrame frame = new JFrame();

        // Vlastni graficky obsah
        Panel panel = new Panel();
        panel.setPreferredSize(new Dimension(800, 600));

        JButton button_1 = new JButton("Zmensi");
        button_1.addActionListener(e -> panel.zmensi());


        JButton button_2 = new JButton("Reset");
        button_2.addActionListener(e -> panel.reset());

        JButton button_3 = new JButton("Konec");
        button_3.addActionListener(e -> frame.dispose());

        JPanel spodni = new JPanel();
        spodni.setBackground(Color.WHITE);
        spodni.add(button_1);
        spodni.add(button_2);
        spodni.add(button_3);

        panel.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                boolean inside = panel.isInsideEllipse(x, y);
                if (predchozi != inside) {
                    panel.setBarva(inside ? Color.RED : Color.YELLOW);
                    panel.repaint();
                    predchozi = inside;
                    System.out.println(System.currentTimeMillis());
                }

            }

            @Override
            public void mouseDragged(MouseEvent e) {
                panel.setCursorCoords(e.getX(), e.getY());
                panel.repaint();
            }
        });

        spodni.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                spodni.setBackground(Color.RED);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                spodni.setBackground(Color.WHITE);
            }

        });

        frame.add(panel);
        frame.add(spodni,BorderLayout.PAGE_END);

        // Standardni manipulace s oknem
        frame.setTitle("Ver_2019/Cviceni_05");
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
