package Ver_2019.Cviceni_04;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/*
4 zakladni operace: posun, rotace, skalovani, zkoseni.
 */
public class Panel extends JPanel {
    private final long cas_spusteni = System.currentTimeMillis();
    private int image_i = 0;
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D)g;

        long ubehlo = System.currentTimeMillis() - cas_spusteni;

        int w = getWidth();
        int h = getHeight();

        g2.translate(w / 2, h / 2);
        g2.setColor(Color.yellow);
        g2.fillOval(-50, -50, 100, 100);
        g2.rotate(2 * Math.PI * ubehlo / 3000);

        drawFlower(g2, ubehlo);


        BufferedImage image = null;
        try {
            image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            ImageIO.write(image, "png", new File("C:\\Users\\kravtsov\\Downloads\\Test11\\screenshot_" + image_i + ".png"));
        } catch (AWTException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        image_i++;
    }

    public void drawFlower(Graphics2D g2, double ubehlo) {

        for (int i = 0; i < 6; i++) {
            AffineTransform obecny = g2.getTransform();
            g2.translate(100, 0);
            g2.setColor(Color.green);
            g2.fillOval(-25, -25, 50, 50);

            AffineTransform VK = g2.getTransform();
            g2.rotate(Math.toRadians(ubehlo/10));
            g2.setColor(Color.BLUE);
            g2.translate(20,0);
            g2.fillOval(0, 0, 15, 15);
            g2.setTransform(VK);


            g2.translate(-100, 0);
            g2.setTransform(obecny);
            g2.rotate(Math.toRadians(60));
        }
    }

    public void drawCoords(Graphics2D g) {
        Color c = g.getColor();
        g.setColor(Color.RED);
        drawArrrow(g, 0, 0, 50, 0);
        g.setColor(Color.BLUE);
        drawArrrow(g, 0, 0, 0, 50);
        g.setColor(c);
    }

    private void drawChessBoard(Graphics2D g) {
        int boardSize = 400;
        int blockSize = boardSize / 8;
        int blocksCount = boardSize/blockSize;

        int x = 0, y = 0;

        for (int i = 0; i < blocksCount; i++) {
            for (int j = 0; j < blocksCount; j++) {
                if ((i + j) % 2 == 0) {
                    g.setColor(Color.BLACK);
                } else {
                    g.setColor(Color.WHITE);
                }
                g.fillRect(x, y, blockSize, blockSize);
                x += blockSize;
            }
            x = 0;
            y += blockSize;
        }
    }

    private void drawArrrow(Graphics2D g, int ax, int ay, int bx, int by) {
       int k = 10;
       int l = 5;

        g.drawLine(ax, ay, bx, by);

        int ux = ax-bx;
        int uy = ay-by;

        double d = Math.hypot(ux, uy);
        double ux1 = ux/d;
        double uy1 = uy/d;

        double cx = bx+k*ux1;
        double cy = by+k*uy1;

        double nx1 = uy1;
        double ny1 = -ux1;

        double d1x = cx+nx1*l;
        double d1y = cy+ny1*l;
        double d2x = cx-nx1*l;
        double d2y = cy-ny1*l;
        g.draw(new Line2D.Double(bx, by, d1x, d1y));
        g.draw(new Line2D.Double(bx, by, d2x, d2y));
    }
}
