package Ver_2019.Cviceni_07;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.AttributedString;

public class Panel extends JPanel {
    BufferedImage image = null;
    public Panel() {
        System.out.println("Jsem to ja");

        try {
            image = ImageIO.read(new File("./cv07_data/img_1.jpg"));
        } catch (IOException e) {
            System.err.println("Chyba");
        }
        System.out.println("w = " + image.getWidth());
        paintMyImage(image.createGraphics(), image.getWidth(), image.getHeight());

        try {
            ImageIO.write(image, "png", new File("./cv07_data/out.png"));
        } catch (IOException e) {
            System.out.println("Chyba");
        }
    }


    private static void resetBackGround(int w, int h, Graphics2D g) {
        Color color = g.getColor();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, w, h);
        g.setColor(color);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        int w = getWidth();
        int h = getHeight();
        paintMyImage(g, w, h);
        g.drawImage(image, 0, 0, w, h, null);
    }

    private static void paintMyImage(Graphics g, int w, int h) {
        Graphics2D g2 = (Graphics2D) g;

//        resetBackGround(w, h, g2); //Smazani pozadi

//        g2.translate(w / 2, h / 2);
//        int scale = Math.min(w, h);
//        g2.scale(scale, scale);
//        g2.setColor(Color.GRAY);
//        Ellipse2D el = new Ellipse2D.Double(-100, -100, 200, 200); // Uprostred okna
//
//        Area area_1 = new Area(el);
//        Area area_2;
//
//        GeneralPath path = new GeneralPath();
//        path.moveTo(0, 0);
//        path.lineTo(10, -10);
//        path.lineTo(20, 0);
//        path.lineTo(10, 10);
//        path.lineTo(-10, -10);
//        path.lineTo(-20, 0);
//        path.lineTo(-10, 10);
//        path.closePath();
//        area_2 = new Area(path);
//        area_1.subtract(area_2);
//        g2.fill(area_1);

        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//        g2.drawImage(image, 0, 0, w, h, null);


        String caption = "My Own Watermark";
        Color color = new Color(255, 255, 255, 75);
        g2.setColor(color);
        AttributedString as = new AttributedString(caption);
        as.addAttribute(TextAttribute.FAMILY, "Times New Roman");
        as.addAttribute(TextAttribute.SIZE, 24);
        as.addAttribute(TextAttribute.FOREGROUND, color, 1, 2);
        as.addAttribute(TextAttribute.SUPERSCRIPT, TextAttribute.SUPERSCRIPT_SUB, 1, 2);
        g2.drawString(as.getIterator(), 500, 200);

    }

}
