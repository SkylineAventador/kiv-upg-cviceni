package Ver_2019.Cviceni_06;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.*;

public class Panel extends JPanel {
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D)g;

        int w = getWidth();
        int h = getHeight();

        g2.translate(w / 2, h / 2);
        GeneralPath path = new GeneralPath();
        path.moveTo(-20, 0);
        path.lineTo(0,-20);
        path.lineTo(20,0);
        path.lineTo(0,20);
        path.closePath();
        path.moveTo(-10,0);
//        path.lineTo(0,10);
//        path.lineTo(10,0);
        path.curveTo(0, 10, 0, 10, 10, 0);
//        path.lineTo(0,-10);
        path.curveTo(0, -10, 0, -10, -10, 0);
        path.closePath();
        Ellipse2D ellipse2D = new Ellipse2D.Double(-150, -150, 300, 300);
        Area el_area = new Area(ellipse2D);
        Area path_area = new Area(path);

        path_area.transform(AffineTransform.getTranslateInstance(50, -70));
        el_area.subtract(path_area);

        GeneralPath mouth_path = new GeneralPath();
        mouth_path.moveTo(0,0);
        mouth_path.lineTo(500, -200);
        mouth_path.lineTo(500, 200);
        mouth_path.closePath();
        Area aaa = new Area(mouth_path);
        el_area.subtract(aaa);

        g2.setPaint(new RadialGradientPaint(-60, -60, 100, new float[]{0f, 1f},
                new Color[]{
                        Color.WHITE,
                        new Color(255, 130, 0)
                }));
        g2.fill(el_area);
        g2.setPaint(new LinearGradientPaint(0, -150, 0, 150, new float[]{0f, 0.6f, 1f},
                new Color[]{
                        new Color(0, 0, 0, 0),
                        new Color(0, 0, 0, 50),
                        new Color(0, 0, 0, 200)
                }));
        g2.fill(el_area);
        g2.setClip(el_area);
        g.setColor(Color.BLACK);
        for (int x = -150; x < 150; x+=10) {
            g2.drawLine(x, -150, x, 150);
        }
        g2.setClip(null);
        //For point exercise


        g2.translate(300, 0);
        Area kruh = new Area(new Ellipse2D.Double(-75, -150, 150, 150));
        Area obdelnik = new Area(new Rectangle2D.Double(-74, -75, 150, 175));
        g2.setPaint(new RadialGradientPaint(-35, -100, 120, new float[]{0f, 0.5f,1f},
                new Color[]{
                        Color.WHITE,
                        new Color(0, 102, 255),
                        new Color(0, 61, 153)
                }));

        g2.fill(kruh);
        g2.fill(obdelnik);

        g2.setPaint(new LinearGradientPaint(-35, -100, -35, 100, new float[]{0f, 0.8f, 1f},
                new Color[]{
                        new Color(0, 0, 0, 0),
                        new Color(0, 0, 0, 0),
                        new Color(0, 0, 0, 255)
                }));
        GeneralPath zuby = new GeneralPath();
        zuby.moveTo(-35, 100);
        zuby.lineTo(-20, -100);
        zuby.lineTo(20, 100);
        zuby.closePath();
        Area zuby_area = new Area(zuby);
        obdelnik.subtract(zuby_area);
        g2.fill(obdelnik);
        g2.fill(kruh);
    }
}
