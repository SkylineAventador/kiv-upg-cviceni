package Ver_2019.Cviceni_02;

import javax.swing.*;
import java.awt.*;


/**
 * @author Josef Kohout
 * @author [Replica] Dmytro Kravtsov
 */
public class DrawingPanel extends JPanel {
    // Bez nasledujici definice hlasi prekladac "warning".
    // Tzv. serializaci resit nebudeme, vyuzijeme standardni reseni.
    private static final long serialVersionUID = 1L;

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.setColor(Color.BLACK);
//        g.drawLine(0, 0, 50, 100);
//
//        g.drawLine(0, this.getHeight(), this.getWidth(), 0);
//        System.out.println("W: " + this.getWidth() + " H:" + this.getHeight());
//
//        g.fillRect(100, 0, 30, 30);
//        g.setColor(Color.RED);
//        g.drawRect(100, 0, 30, 30);
//
//        int[] xCoord = {150, 165, 180};
//        int[] yCoord = {30, 0, 30};
//        g.setColor(new Color(30, 150, 10));
//
//        g.fillPolygon(xCoord, yCoord, 3);
//
//        g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 30));
//        g.drawString("Text", 203, 30);
//
//        int circleX = 150;
//        int circleY = 80;
//        int circleRadius = 5;
//
//        g.fillOval(circleX - circleRadius, circleY - circleRadius, 2 * circleRadius, 2 * circleRadius);
//        String text = "Centrovany popisek";
//
//        FontMetrics metrics = g.getFontMetrics(g.getFont());
//        int textHeight = metrics.getHeight();
//        int textWidth = metrics.stringWidth(text);
//
//        g.drawString(text, circleX - (textWidth / 2), circleY + circleRadius + textHeight);
//
//        AttributedString as = new AttributedString("H2O");
//        as.addAttribute(TextAttribute.FAMILY, "Times New Roman");
//        as.addAttribute(TextAttribute.SIZE, 40);
//        as.addAttribute(TextAttribute.FOREGROUND, Color.magenta, 1, 2);
//        as.addAttribute(TextAttribute.SUPERSCRIPT, TextAttribute.SUPERSCRIPT_SUB, 1, 2);
//        g.drawString(as.getIterator(), 10, 180);

//        //Initial rectangle
//        int[] xCoord = {50, 200, 200, 50};
//        int[] yCoord = {200, 200, 50, 50};
//
//        g.drawPolygon(xCoord, yCoord, 4);
//
//        int currentX, currentY;


        g.drawRect(200,200,200,200);
        int x,y,w,h;
        x= 200;
        y = 200;
        w = 200;
        h =200;

        int step = 10;
        for (int i = 0; i < 25; i++) {
            if (i < 25 / 2) {
                x += x/step;
                y += y/step;
                w -= w/step;
                h -= h/step;
            } else {
                x -= x/step;
                y -= y/step;
                w -= w/step;
                h -= h/step;
            }
            g.drawRect(x, y, w, h);
        }

//        int[] xCoord2 = new int[4];
//        int[] yCoord2 = new int[4];
//
//        for (int i = 0; i < 4; i++) {
//            xCoord2[i] = (xCoord[(i + 1) % 4] + xCoord[i]) / 2;
//            yCoord2[i] = (yCoord[(i + 1) % 4] + yCoord[i]) / 2;
//        }
//
//        g.drawPolygon(xCoord2, yCoord2, 4);
    }
}
