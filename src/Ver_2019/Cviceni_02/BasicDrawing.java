package Ver_2019.Cviceni_02;

import javax.swing.*;
import java.awt.*;

public class BasicDrawing {
    public static void main(String[] args) {
        JFrame frame = new JFrame();

        // Do okna pridame svuj obsah
        DrawingPanel drawingPanel = new DrawingPanel();
        drawingPanel.setPreferredSize(new Dimension(640, 480));
        frame.add(drawingPanel);

        // Titulek okna
        frame.setTitle("Ver_2019/Cviceni_02");

//        // Vychozi velikost okna v pixelech
//        // (v budoucnu nebudeme potrebovat)
//        frame.setSize(640, 480);

        // Spocitej velikost okna, aby mely vsechny komponenty preferovanou velikost.
        frame.pack();

        // Se zavrenim okna se ukonci aplikace; standardne se
        // pouze okno skryje
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Umisti okno doprostred obrazovky
        frame.setLocationRelativeTo(null);

        // Zobrazi okno
        frame.setVisible(true);
    }
}
