package Ver_2019.Cviceni_09;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {
//    public Panel() {
//        MySvgGraphics g = new MySvgGraphics();
//        paint(g);
//    }

    @Override
    public void paint(Graphics g) {
        paint(g, getWidth(), getHeight());
    }

    public void paint(Graphics g, int w, int h) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;

        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                int px = w * x / 8;
                int py = h * y / 8;
                int rx = w / 8;
                int ry = h / 8;

                g2.setColor(((x + y) % 2 == 0) ? Color.BLACK : Color.WHITE);
                g2.fillRect(px, py, rx, ry);
            }
        }
    }


    private String svg_text = "";

    public void fillRect(int x, int y, int width, int height) {
        svg_text += "<rect =\"" + x + " y=\"" + y + " width=\"" + width + " height=\"" + height + " />";
    }

    private String getSvg_Element() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<svg width=\"500\" height=\"500\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" " +
                "xmlns:xlink=\"http://www.w3.org/1999/xlink\">" +
                svg_text + "</svg>";
    }

}
