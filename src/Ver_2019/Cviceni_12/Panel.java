package Ver_2019.Cviceni_12;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Panel extends JPanel {
    private static final int X = 256;
    private static final int Y = 320;
    private static final int Z = 128;

    private int idx = 0;

    private byte[] data;
    private BufferedImage[] img = new BufferedImage[Z];

    public Panel() {
        try {
            File f = new File("./cv12_data/riddle.raw");
            FileInputStream fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            data = new byte[(int) f.length()];
            dis.readFully(data);
            dis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int x = 0; x < Z; x++) {
            img[x] = getLayer(x);
        }
        new Timer(20, e -> {
            idx++;
            idx %= Z;
            repaint();
        }).start();
    }

    private BufferedImage getLayer(int x) {
        BufferedImage result = new BufferedImage(Y, Z, BufferedImage.TYPE_INT_RGB);
        int offset = X/3;
        for (int i = 0; i < Z; i++) {
            for (int j = 0; j < Y; j++) {
                byte color = data[offset];
                int clr = ((int) color+j*X) & 0xff;
                result.setRGB(j, i, clr | (clr << 8) | (clr << 16));
            }
            offset += X * Y;
        }
        return result;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(img[idx], 0, 0, null);
    }

}
