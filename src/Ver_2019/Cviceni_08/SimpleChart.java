package Ver_2019.Cviceni_08;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.Arrays;

/**
 * Zbytek je v Eclipsovem projektu na CW.
 */
public class SimpleChart {
    private double[] xData;
    private double[] yData;

    private double[] ticksX;
    private double[] ticksY;

    private double xMin, xMax, yMin, yMax;

    private Graphics2D g2;
    private int chartHeight, chartWidth;

    private int dataMarkSize = 10;
    private int tickCount = 10;
    private int tickLength = 5;

    public SimpleChart(double[] xData, double[] yData) {
        this.xData = xData;
        this.yData = yData;

        xMin = Arrays.stream(xData).min().getAsDouble();
        xMax = Arrays.stream(xData).max().getAsDouble();
        yMin = Arrays.stream(yData).min().getAsDouble();
        yMax = Arrays.stream(yData).max().getAsDouble();

        ticksX = new double[tickCount];
        double dt = (1 / ((double) tickCount - 1.0))
                * (xMax - xMin);

        for (int i = 0; i < ticksX.length; i++) {
            ticksX[i] = xMin + i * dt;
        }
    }

    public void draw(Graphics2D g2, int width, int height) {
        g2.drawRect(0, 0, chartWidth, chartHeight);

        g2.translate(20, 20); // Pozdeji jako promennou zadavat.

        this.g2 = g2;
        this.chartWidth = width - 40; //Taky
        this.chartHeight = height - 40; //Taky

        drawData();
        drawAxes();
    }

    private void drawAxes(){
        g2.drawRect(0, 0, chartWidth, chartHeight);
        for (int i = 0; i < ticksX.length; i++) {
            double x = getConvX(ticksX[i]);
            g2.draw(new Line2D.Double(x, chartHeight, x, chartHeight + tickLength));
        }
    }

    private void drawData() {
        g2.setColor(Color.RED);
        for (int i = 0; i < this.xData.length; i++) {
            g2.fill(new Ellipse2D.Double(getConvX(this.xData[i]) - dataMarkSize * 0.5,
                    getConvY(this.yData[i]) - dataMarkSize * 0.5, dataMarkSize, dataMarkSize));
        }
        g2.setColor(Color.BLACK);
    }

    private double getConvX(double x) {
        return (x - xMin) * (chartWidth / (xMax - xMin));
    }

    private double getConvY(double y) {
        return chartHeight - (y - yMin) * (chartHeight / (yMax - yMin));
    }
}
