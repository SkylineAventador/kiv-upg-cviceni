package Ver_2019.Cviceni_08;

import javax.swing.*;
import java.awt.*;

public class Panel extends JPanel {
    private SimpleChart chart;

    public Panel(SimpleChart chart) {
        this.chart = chart;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        int w = this.getWidth();
        int h = this.getHeight();
        Graphics2D g2 = (Graphics2D)g;

        g2.translate(100, 100);
        chart.draw(g2, w - 200, h - 200);
    }
}
