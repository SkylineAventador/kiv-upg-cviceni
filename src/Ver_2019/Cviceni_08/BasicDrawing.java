package Ver_2019.Cviceni_08;

import javax.swing.*;
import java.awt.*;

public class BasicDrawing {
    public static void main(String[] args) {
        SimpleChart chart = new SimpleChart(
                new double[]{0.5, 10, 12.5, 17},
                new double[]{5, 0.2, 12.5, 10}
        );
        JFrame frame = new JFrame();
        Panel panel = new Panel(chart);
        panel.setPreferredSize(new Dimension(800, 600));
        frame.add(panel);

        // Standardni manipulace s oknem
        frame.setTitle("Ver_2019/Cviceni_08");
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
